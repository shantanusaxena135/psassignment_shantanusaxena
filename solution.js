// This file contains solution as well as the tests (second part of the file) in response to the coding given by pluralsight

/**
 * Converts given array of strings into hash map with key and value split from each string delimited by ": ".
 * @param {array} dependencyArray - Array of string containing names of package to be installed.
 * @return {hash} hash with key as dependent package and value as package to which key package is dependent.
 */
function parseHash(dependencyArray) {
	var keyValueHash = new Object();
    for(i = 0; i < dependencyArray.length; i++) {
		keyValue = dependencyArray[i].split(': ')
		if(keyValue[0].length != 0) {
			keyValueHash[keyValue[0]] = keyValue[1]
		}
	}
    return keyValueHash;
}

/**
 * Checks for circular dependency for the packages in dependencyHash passed as argument. If circular dependency is found, clears the resolvedDependencyList array and insert a string '--'
 * Otherwise, insert the package names in the correct order in resolvedDependencyList array.
 * @param {string} key - the package name to be resolved for dependencies.
 * @param {hash} dependencyHash - with key as dependent package and value as package to which key package is dependent.
 * @param {array} resolvedDependencyList - the final dependency list in the correct dependency order
 */
function addDependency(key, dependencyHash, resolvedDependencyList) {
	//will be used to determine circular dependency 
	var tempDependencyList = [key];
	//while string from key/value pair in dependencyArray was not empty and parsed into undefined
	while (key != undefined) {
		// if circular dependency is found
		if(tempDependencyList[0] == dependencyHash[key]) {
			resolvedDependencyList.length = 0
			resolvedDependencyList.push('--');
			return 0;
		}
		else{
			if (dependencyHash[key] != undefined) {
				tempDependencyList.push(dependencyHash[key]);
			}
		}
		key = dependencyHash[key];
	}
	
	for(i = tempDependencyList.length-1; i >= 0 ; i--) {
		if(resolvedDependencyList.indexOf(tempDependencyList[i]) == -1 && tempDependencyList[i].length > 0)
		resolvedDependencyList.push(tempDependencyList[i]);
	}	
}


/**
 * Resolves the package dependency into a string and detects any circular dependencies in case.
 * @param {array} dependencyArray - Array of string containing names of package to be installed.
 * @return {string} string containing package dependencies in order for installation.
 * returns empty string for circular dependency
 */
function getResolvedDependencies(dependencyArray) {
	var dependencyHash = parseHash(dependencyArray);
	var resolvedDependencyList = [];
	for (var key in dependencyHash) {
	    if (dependencyHash.hasOwnProperty(key)) {
	    	addDependency(key, dependencyHash, resolvedDependencyList)
	    }
	}
	if(resolvedDependencyList[0] == '--') {
		console.log("Circular Dependency");
		return '';
	}
	console.log(resolvedDependencyList.join(', '));
	return resolvedDependencyList.join(', ');
}

// ########################################### Tests #####################################################

var assert = require("assert");

//test cases for function #parseHash
expectedInputOne = ['key1: value1', 'key2: value2'];
expectedOutputOne = {"key1": 'value1', "key2": 'value2'};
assert.deepEqual(parseHash(expectedInputOne), expectedOutputOne, "Parses atring in array into Hash" );

expectedInputTwo = ['key1: ', 'key2: value2'];
expectedOutputTwo = {"key1": '', "key2": 'value2'};
assert.deepEqual(parseHash(expectedInputTwo), expectedOutputTwo, "Parses string in array with supposed value as empty string into Hash" );

expectedInputThree = [': value1', 'key2: value2'];
expectedOutputThree = {"key2": 'value2'};
assert.deepEqual(parseHash(expectedInputThree), expectedOutputThree, "Does not parses string in array with supposed value as empty string into Hash" );





//test cases for function #addDependency
inputKey  = 'Fraudstream';
inputDependencyHash = { 'KittenService': '',
						 'Leetmeme': 'Cyberportal',
						 'Cyberportal': 'Ice',
						 'CamelCaser': 'KittenService',
						 'Fraudstream': 'Leetmeme',
						 'Ice': '' }

inputResolvedDependencyList = []
expectedResolvedDependencyList = ['Ice', 'Cyberportal', 'Leetmeme', 'Fraudstream']
addDependency(inputKey, inputDependencyHash, inputResolvedDependencyList);
assert.deepEqual(inputResolvedDependencyList, expectedResolvedDependencyList, "Successfully inserts package names in correct order into resolvedDependencyList for one particular package");


inputKey  = 'KittenService';
inputResolvedDependencyList = []
expectedResolvedDependencyList = ['KittenService']
addDependency(inputKey, inputDependencyHash, inputResolvedDependencyList);
assert.deepEqual(inputResolvedDependencyList, expectedResolvedDependencyList, "Inserts only package name(key) if its not dependent to another package");

inputKey  = 'Cyberportal';
inputDependencyHash = { 'KittenService': '',
						 'Leetmeme': 'Cyberportal',
						 'Cyberportal': 'Ice',
						 'CamelCaser': 'KittenService',
						 'Fraudstream': '',
						 'Ice': 'Leetmeme' }
inputResolvedDependencyList = []
expectedResolvedDependencyList = ['--']
addDependency(inputKey, inputDependencyHash, inputResolvedDependencyList);
assert.deepEqual(inputResolvedDependencyList, expectedResolvedDependencyList, "Detects circular dependency and insert '--' in resolvedDependencyList");

inputKey  = 'Fraudstream';
inputDependencyHash = { 'KittenService': '',
						 'Leetmeme': 'Cyberportal',
						 'Cyberportal': 'Ice',
						 'CamelCaser': 'KittenService',
						 'Fraudstream': 'Leetmeme',
						 'Ice': 'Leetmeme' }
inputResolvedDependencyList = []
expectedResolvedDependencyList = ['--']
addDependency(inputKey, inputDependencyHash, inputResolvedDependencyList);
assert.deepEqual(inputResolvedDependencyList, expectedResolvedDependencyList, "Detects circular dependency from multiple packages to the same package");
//





// test cases for #getResolvedDependencies function
inputOne = [ "KittenService: CamelCaser", "CamelCaser: " ]
expectedOutput = "CamelCaser, KittenService"
assert.deepEqual(getResolvedDependencies(inputOne), expectedOutput, "Succesfully resolves single-level dependencies(array of string) into a string")


inputTwo = ["KittenService: ",
            "Leetmeme: Cyberportal",
            "Cyberportal: Ice",
            "CamelCaser: KittenService",
            "Fraudstream: Leetmeme",
            "Ice: " ]
expectedOutput = "KittenService, Ice, Cyberportal, Leetmeme, CamelCaser, Fraudstream"
assert.deepEqual(getResolvedDependencies(inputTwo), expectedOutput, "Succesfully resolves multi-level dependencies(array of string) into a string")


inputThree = ["KittenService: ",
            "Leetmeme: Cyberportal",
            "Cyberportal: Ice",
            "CamelCaser: KittenService",
            "Fraudstream: ",
            "Ice: Leetmeme"  ]
expectedOutput = ""
assert.deepEqual(getResolvedDependencies(inputThree), expectedOutput, "Detects circular dependency from single path")

inputThree = ["KittenService: ",
            "Leetmeme: Cyberportal",
            "Cyberportal: Ice",
            "CamelCaser: KittenService",
            "Fraudstream: Leetmeme",
            "Ice: Leetmeme" ]
expectedOutput = ""
assert.deepEqual(getResolvedDependencies(inputThree), expectedOutput, "Detects circular dependency from muliple path")
// This file contains solution as well as the tests (second part of the file) in response to the coding given by pluralsight

/**
 * Converts given array of strings into hash map with key and value split from each string delimited by ": ".
 * @param {array} dependencyArray - Array of string containing names of package to be installed.
 * @return {hash} hash with key as dependent package and value as package to which key package is dependent.
 */
function parseHash(dependencyArray) {
	var keyValueHash = new Object();
    for(i = 0; i < dependencyArray.length; i++) {
		keyValue = dependencyArray[i].split(': ')
		if(keyValue[0].length != 0) {
			keyValueHash[keyValue[0]] = keyValue[1]
		}
	}
    return keyValueHash;
}

/**
 * Checks for circular dependency for the packages in dependencyHash passed as argument. If circular dependency is found, clears the resolvedDependencyList array and insert a string '--'
 * Otherwise, insert the package names in the correct order in resolvedDependencyList array.
 * @param {string} key - the package name to be resolved for dependencies.
 * @param {hash} dependencyHash - with key as dependent package and value as package to which key package is dependent.
 * @param {array} resolvedDependencyList - the final dependency list in the correct dependency order
 */
function addDependency(key, dependencyHash, resolvedDependencyList) {
	//will be used to determine circular dependency 
	var tempDependencyList = [key];
	//while string from key/value pair in dependencyArray was not empty and parsed into undefined
	while (key != undefined) {
		// if circular dependency is found
		if(tempDependencyList[0] == dependencyHash[key]) {
			resolvedDependencyList.length = 0
			resolvedDependencyList.push('--');
			return 0;
		}
		else{
			if (dependencyHash[key] != undefined) {
				tempDependencyList.push(dependencyHash[key]);
			}
		}
		key = dependencyHash[key];
	}
	
	for(i = tempDependencyList.length-1; i >= 0 ; i--) {
		if(resolvedDependencyList.indexOf(tempDependencyList[i]) == -1 && tempDependencyList[i].length > 0)
		resolvedDependencyList.push(tempDependencyList[i]);
	}	
}


/**
 * Resolves the package dependency into a string and detects any circular dependencies in case.
 * @param {array} dependencyArray - Array of string containing names of package to be installed.
 * @return {string} string containing package dependencies in order for installation.
 * returns empty string for circular dependency
 */
function getResolvedDependencies(dependencyArray) {
	var dependencyHash = parseHash(dependencyArray);
	var resolvedDependencyList = [];
	for (var key in dependencyHash) {
	    if (dependencyHash.hasOwnProperty(key)) {
	    	addDependency(key, dependencyHash, resolvedDependencyList)
	    }
	}
	if(resolvedDependencyList[0] == '--') {
		console.log("Circular Dependency");
		return '';
	}
	console.log(resolvedDependencyList.join(', '));
	return resolvedDependencyList.join(', ');
}

// ########################################### Tests #####################################################

var assert = require("assert");

//test cases for function #parseHash
expectedInputOne = ['key1: value1', 'key2: value2'];
expectedOutputOne = {"key1": 'value1', "key2": 'value2'};
assert.deepEqual(parseHash(expectedInputOne), expectedOutputOne, "Parses atring in array into Hash" );

expectedInputTwo = ['key1: ', 'key2: value2'];
expectedOutputTwo = {"key1": '', "key2": 'value2'};
assert.deepEqual(parseHash(expectedInputTwo), expectedOutputTwo, "Parses string in array with supposed value as empty string into Hash" );

expectedInputThree = [': value1', 'key2: value2'];
expectedOutputThree = {"key2": 'value2'};
assert.deepEqual(parseHash(expectedInputThree), expectedOutputThree, "Does not parses string in array with supposed value as empty string into Hash" );





//test cases for function #addDependency
inputKey  = 'Fraudstream';
inputDependencyHash = { 'KittenService': '',
						 'Leetmeme': 'Cyberportal',
						 'Cyberportal': 'Ice',
						 'CamelCaser': 'KittenService',
						 'Fraudstream': 'Leetmeme',
						 'Ice': '' }

inputResolvedDependencyList = []
expectedResolvedDependencyList = ['Ice', 'Cyberportal', 'Leetmeme', 'Fraudstream']
addDependency(inputKey, inputDependencyHash, inputResolvedDependencyList);
assert.deepEqual(inputResolvedDependencyList, expectedResolvedDependencyList, "Successfully inserts package names in correct order into resolvedDependencyList for one particular package");


inputKey  = 'KittenService';
inputResolvedDependencyList = []
expectedResolvedDependencyList = ['KittenService']
addDependency(inputKey, inputDependencyHash, inputResolvedDependencyList);
assert.deepEqual(inputResolvedDependencyList, expectedResolvedDependencyList, "Inserts only package name(key) if its not dependent to another package");

inputKey  = 'Cyberportal';
inputDependencyHash = { 'KittenService': '',
						 'Leetmeme': 'Cyberportal',
						 'Cyberportal': 'Ice',
						 'CamelCaser': 'KittenService',
						 'Fraudstream': '',
						 'Ice': 'Leetmeme' }
inputResolvedDependencyList = []
expectedResolvedDependencyList = ['--']
addDependency(inputKey, inputDependencyHash, inputResolvedDependencyList);
assert.deepEqual(inputResolvedDependencyList, expectedResolvedDependencyList, "Detects circular dependency and insert '--' in resolvedDependencyList");

inputKey  = 'Fraudstream';
inputDependencyHash = { 'KittenService': '',
						 'Leetmeme': 'Cyberportal',
						 'Cyberportal': 'Ice',
						 'CamelCaser': 'KittenService',
						 'Fraudstream': 'Leetmeme',
						 'Ice': 'Leetmeme' }
inputResolvedDependencyList = []
expectedResolvedDependencyList = ['--']
addDependency(inputKey, inputDependencyHash, inputResolvedDependencyList);
assert.deepEqual(inputResolvedDependencyList, expectedResolvedDependencyList, "Detects circular dependency from multiple packages to the same package");
//





// test cases for #getResolvedDependencies function
inputOne = [ "KittenService: CamelCaser", "CamelCaser: " ]
expectedOutput = "CamelCaser, KittenService"
assert.deepEqual(getResolvedDependencies(inputOne), expectedOutput, "Succesfully resolves single-level dependencies(array of string) into a string")


inputTwo = ["KittenService: ",
            "Leetmeme: Cyberportal",
            "Cyberportal: Ice",
            "CamelCaser: KittenService",
            "Fraudstream: Leetmeme",
            "Ice: " ]
expectedOutput = "KittenService, Ice, Cyberportal, Leetmeme, CamelCaser, Fraudstream"
assert.deepEqual(getResolvedDependencies(inputTwo), expectedOutput, "Succesfully resolves multi-level dependencies(array of string) into a string")


inputThree = ["KittenService: ",
            "Leetmeme: Cyberportal",
            "Cyberportal: Ice",
            "CamelCaser: KittenService",
            "Fraudstream: ",
            "Ice: Leetmeme"  ]
expectedOutput = ""
assert.deepEqual(getResolvedDependencies(inputThree), expectedOutput, "Detects circular dependency from single path")

inputThree = ["KittenService: ",
            "Leetmeme: Cyberportal",
            "Cyberportal: Ice",
            "CamelCaser: KittenService",
            "Fraudstream: Leetmeme",
            "Ice: Leetmeme" ]
expectedOutput = ""
assert.deepEqual(getResolvedDependencies(inputThree), expectedOutput, "Detects circular dependency from muliple path")
