# README #

This is a Node JS project in response to the coding exercise given by Plural Sight.
Solution submitted by Shantanu Saxena.

### Prerequisites:

- Node JS 6.10.3

### Installation

* Download and install nodejs compatible to the machine.
https://nodejs.org/en/download/

### How to run? ###

* Clone the project from https://shantanusaxena135@bitbucket.org/shantanusaxena135/psassignment_shantanusaxena.git
* Go to the root directory, for this project - PSAssignmnet_ShantanuSaxena.
* run node solution.js

### Test ###
The tests are also in the same file solution.js. The second part of the file covers the test cases. Testing is done by using the package 'assert'.

### FOR PLURAL SIGHT EXAMINATION ###
The solution is divided into three functions:

1. parseHash
2. addDependency
3. getResolvedDependencies

The master driving function is #getResolvedDependencies where the examiner can apply the test cases. 
The function is expected to return the string containing order of installation for packages or empty string for circular dependency.